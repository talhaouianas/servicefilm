import com.film.service.FilmService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FilmServiceTest {

    @Test
    public void testGetNumberOfFilmsByGenre() {
        FilmService filmService = new FilmService();
        filmService.loadFilms("films.json");
        assertEquals(2, filmService.getNumberOfFilmsByGenre("Sci-Fi"));
    }

    // Vous pouvez ajouter d'autres tests unitaires ici
}
