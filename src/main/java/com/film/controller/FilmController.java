package com.film.controller;


import com.film.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/films")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @PostMapping
    public void uploadFilms() {
        filmService.uploadFilms();
    }

    @GetMapping("/genre/{name}")
    public int getNumberOfFilmsByGenre(@PathVariable String name) {
        return filmService.getNumberOfFilmsByGenre(name);
    }

    // Ajouter d'autres endpoints ici pour l'API OMDBAPI
}