package com.film.models;

public class Film {
    private String Title;
    private String Year;
    private String Genre;
    private String Type;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}