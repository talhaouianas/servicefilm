package com.film.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.film.models.Film;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class FilmService {

    private List<Film> films;

    public void uploadFilms() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            films = objectMapper.readValue(new File("src/main/resources/films.json"),
                    new TypeReference<List<Film>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getNumberOfFilmsByGenre(String name) {
        int count = 0;
        for (Film film : films) {
            if (film.getTitle().toLowerCase().contains(name.toLowerCase())) {
                count++;
            }
        }
        return count;
    }

}
