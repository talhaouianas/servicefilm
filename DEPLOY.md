## Configuration requise

Assurez-vous que le système dispose des éléments suivants :

- Java Development Kit (JDK) version 8 ou supérieure installé
- Maven pour la gestion des dépendances
- Accès à internet pour l'API OMDB

## Instructions de déploiement

1. **Cloner le référentiel** 

   ```
   git clone interview
   ```

2. **Importer dans votre IDE**

   Ouvrez votre IDE (Eclipse, IntelliJ, etc.) et importez le projet en tant que projet Maven.

3. **Configuration du fichier films.json**

   Assurez-vous que le fichier `films.json` est correctement configuré avec les détails des films à traiter.

4. **Exécuter l'application**

   Depuis votre IDE, exécutez la classe `Springbootapplication.java` pour lancer l'application Spring Boot.

5. **Tester les endpoints**

   Utilisez des outils comme Postman ou curl pour tester les endpoints exposés par l'API.

6. **Déploiement sur un serveur distant**

   Si vous souhaitez déployer l'API sur un serveur distant, générez un fichier JAR exécutable à l'aide de la commande Maven suivante :

   ```
   mvn clean package
   ```


   Assurez-vous que le serveur dispose de Java installé et des autorisations nécessaires pour exécuter le fichier JAR.

7. **Vérification du déploiement**

   Vérifiez que l'API est accessible à l'aide de l'URL et des endpoints définis dans l'application.

---

